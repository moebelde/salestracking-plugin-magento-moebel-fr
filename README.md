Magento 2 Salestracking by Meubles
===========================================

[![Latest Stable Version]](https://packagist.org/packages/moebelde/module-meubles-salestracking)
[![Total Downloads]](https://packagist.org/packages/moebelde/module-meubles-salestracking)

## How to install moebel sales tracking module

### 1. Install via composer (recommend)

We recommend you to install moebelde/module-meubles-salestracking module via composer. It is easy to install, update and maintaince.

Run the following command in Magento 2 root folder.

#### 1.1 Install

```
composer require moebelde/module-meubles-salestracking
php bin/magento setup:upgrade
bin/magento module:enable Meubles_Salestracking
php bin/magento setup:di:compile
bin/magento cache:clean
```


#### Support
v1.0.0 - Magento 2.2.*, 2.3.*, 2.4.*