<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the moebel.de Einrichten & Wohnen AG License
 * that is available through the world-wide-web at this URL:
 * https://sales1.moebel.de/licenses/magento.txt
 */

/**
 * @category   Sales Tracking
 * @package    Meubles_Salestracking
 * @subpackage etc
 * @author     meubles.fr <salestracking@meubles.fr>
 * @copyright  Copyright (c) 2020 moebel.de Einrichten & Wohnen AG (https://www.moebel.de)
 * @link       https://www.meubles.fr
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Meubles_Salestracking',
    __DIR__
);
